# README #

*Summary:
*This project was developed using the base project provided.

1. It uses Kotlin as programming language
2. The approach used was to make the project as simple as possible, using coroutines as thread to fetch all the information from the API (the calls are short enough to be handled this way)
3. For the images, I used Glide external library as it loads the images quickly enough and it has its own cahing process
4. The rate view implementation creates a three circular stroke image, one for the current rate value which on creation can be "filled" to a specific float value, one that is always "filled" representing 100% rate but with darker color, and one for the backgound circle image.
5. The text for the rate view is managed as a text view directly on the layout
6. As mentioned on the "issues" section, I wasn't able to inspect the provided layouts so I used aproximate values for the images and texts
7. I decided to use card view for the popular movies display as it provides a good view option
8. I took the design provided as base to create my version of the app

# Missing features: #
1. No table to add offline functionality due to timing, this would be solved by:

	* Create a local database
	* Validate if app is connected to internet
	* If it's connected, clear database, load all the items from api and save them on database
	* If it's not connected, load all the items in the database
	* If is the first launch and the database is empty, display a messsage that needs to acces internet
